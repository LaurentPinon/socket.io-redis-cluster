
module.exports = function(io) {
    let adminNsp = io.of('/admin');

    // Set socket connection
    adminNsp.on('connection',function(socket){
        console.log('Admin connected to socket : ' + socket.id);

        adminNsp.adapter.remoteJoin(socket.id, "room1", (err) => {
            if (err) { /* unknown id */ }
            console.log('Joined room : '+ "room1");
        });

        socket.on('disconnect',(data) =>{
            console.log('Admin disconnected');
        })

        socket.on('get_all_rooms',() =>{
            console.log("Receive event : get_all_rooms ");
            adminNsp.adapter.allRooms((err, rooms) => {
                if(err)console.log(err)
                console.log(rooms);
              }); 
        })
    })
}
