// --------------------------------
// -------- Express Setup ---------
// --------------------------------

console.log("Starting node-socket.io");

// -------------------------------
// --- Redis + Socket.io setup ---
// -------------------------------

// --- REDIS-STANDALONE ---
/*
const io = require('socket.io')(4000);
const redisAdapter = require('socket.io-redis');
io.adapter(redisAdapter({ host: process.env.REDIS_HOST , port: process.env.REDIS_PORT }));
*/
// --- IO-REDIS-CLUSTER --
const io = require('socket.io')(4000)
const redisAdapter = require('socket.io-redis');
const IoRedis = require('ioredis');

const startupNodes = [
  {
    port: 6379,
    host: 'redis-1'
  },
  {
    port: 6379,
    host: 'redis-2'
  },
  {
    port: 6379,
    host: 'redis-3'
  }
];

io.adapter(redisAdapter({
  pubClient: new IoRedis.Cluster(startupNodes),
  subClient: new IoRedis.Cluster(startupNodes),
  requestsTimeout: 5000
}));

// -------------------------------
// ------ Socket Namespaces ------
// -------------------------------

require('./socket/admin')(io);  // Admin

// -------------------------------
