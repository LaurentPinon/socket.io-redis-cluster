# Socket.io redis cluster
A simple project to learn how to use socket.io with a cluster of redis.

I was working on socket.io and redis when I noticed some strange behaviors. I decided to create a little project in order to confirm the problems... 

This project uses docker to build 9 containers:

- 3 redis for the cluster
- 1 redis (standalone)
- 1 frontend in vuejs
- 3 servers in nodejs
- 1 Nginx to balance the traffic

## Installation
### Redis environment
First, we will build the network in charge of the redis environment.
Open the redis-cluster folder and run :

```sh
    docker-compose up --build
```

Check that all the containers are running:

```sh
    docker-compose ps
```
You should see :

```
      Name                    Command               State    Ports
--------------------------------------------------------------------
redis-1            docker-entrypoint.sh redis ...   Up      6379/tcp
redis-2            docker-entrypoint.sh redis ...   Up      6379/tcp
redis-3            docker-entrypoint.sh redis ...   Up      6379/tcp
redis-standalone   docker-entrypoint.sh redis ...   Up      6379/tcp
```

As redis doesn't support hostname, to build the cluster we need the ip of the containers.

In the docker-compose.yml, a label is attached to every redis container ( role=redis-cluster ).

Use the following command to have the ip:port of the containers with the label :
    
```sh
    ip=$(docker ps -q -f "label=role=redis-cluster" | xargs -n 1 docker inspect --format '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}:6379')
```

Then display the result with :
```sh
    echo $ip
```
It should be like :  X.X.X.X:6379 X.X.X.X:6379 X.X.X.X:6379 

`Warning : if you stop the containers by using docker-compose down, and then use up to start them again, their ip may have changed. `

Copy the list of ip:port and open a redis container ( the one that you want, I use the redis-1) :
```sh
    docker exec -it redis-1 sh
```

Build the cluster using the following command with the list of ip:port previously copied :
```sh
    redis-cli --cluster create X.X.X.X:6379 X.X.X.X:6379 X.X.X.X:6379 --cluster-replicas 0
```

If everything goes well, you should see :
```
[OK] All nodes agree about slots configuration.
>>> Check for open slots...
>>> Check slots coverage...
[OK] All 16384 slots covered.
```
### Start nginx, the front and the servers:
Come back to the root of the project and run :

```sh
    docker-compose up --build
```

If everything goes well, you should be able to open your browser ( http://localhost:3050/ ) and see :
```
 Welcome to the admin page, connected : true
```

In the page you can see a button "Get all rooms", this button emits an event which should print all the rooms created in the terminal.
This is where I have seen strange behaviors, see repport.md